# Fossilizer

*A fossil-crafting simulator for Path of Exile*

https://fossilizer.gitlab.io/

## FAQ

*(Nobody has actually asked me any of these questions yet)

### How do I use this page?

Pick the fossils you want to use from the drop-down menu on top and the base
you want to use the fossil on from the drop-down on the right.  You'll see
three listings:

**Likely** are mods that the fossils add, e.g. "More Lightning modifiers".
Note that this doesn't mean they're guaranteed, just much more likely to appear.

**Less Likely** (but still possible) are all the mods that haven't been
encouraged by a fossil, but also haven't been ruled out.  Essentially these are
the sort of things you'd see if you used a chaos orb.

**Forbidden** are all the mods that fossils have ruled out, e.g. "No Physical
modifiers".

### If that's all I need to do, what's all that 'fossil' stuff on the left
with the tags and things?

That's so you don't have to remember which fossil does what or search through
the list visually.  Hit `+` next to "Lightning" and the fossils list will
narrow down to those that add lightning.  Hit `-` next to "Fire" and you'll
get a list of those that add lightning and those that prevent fire.  Press the
`+` button next to the fossil to add it to your resonator.

### What doesn't work?

Maps, abyss jewels, regular jewels, flasks (can flasks even *be* fossil
crafted?), basically anything you can't equip as a
weapon/armor/belt/ring/amulet.

### What's 'angular CLI boilerplate'?

Angular is the framework I used to make the site.  It provides a default README
file, and I've preserved it below in case it's useful to anyone.

# Angular CLI boilerplate follows:

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.2.1.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
