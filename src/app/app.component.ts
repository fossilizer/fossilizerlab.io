import { Component } from '@angular/core';
import {Fossil} from './fossils/fossil';
import {FossilService} from './fossils/fossil.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public chosenFossils = [];
  public numFossils = 1;
  public maxFossils = 4;

  constructor(private fossilService: FossilService) {

  }

  // Apparently angular's ngFor can't iterate over just plain
  // numbers, so....
  public fossilIndices(): number[] {
    const result = [];
    for (let i = 0; i < this.numFossils; i++) {
      result.push(i);
    }
    return result;
  }

  public allFossils(): Fossil[] {
    return this.fossilService.fossils;
  }

  public onAddFossil(fossil: Fossil): void {
    for (let index = 0; index < this.maxFossils; index++) {
      if (index >= this.numFossils) {
        this.chosenFossils[index] = fossil.name;
        this.numFossils += 1;
        return;
      }
      if (!this.chosenFossils[index]) {
        this.chosenFossils[index] = fossil.name;
        return;
      }
    }
  }

  public fossilReport(): void {
    console.log("LISTING");
    for (let index = 0; index < this.maxFossils; index++) {
      const displayed = index < this.numFossils;
      console.log(this.chosenFossils[index], displayed ? "onscreen" : "hidden");
    }
  }

  public reset(): void {
    for (let index = 0; index < this.maxFossils; index++) {
      this.chosenFossils[index] = undefined;
    }
    this.numFossils = 1;
  }
}
