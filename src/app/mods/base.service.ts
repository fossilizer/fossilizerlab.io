import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Base, BaseTemplate} from './base';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BaseService {

  public bases: Base[];
  protected baseByName: {[baseName: string]: Base}

  constructor(
    private http: HttpClient,
  ) {
    this.bases = [];
    this.baseByName = {};
    this.getJson().subscribe(data => {
      this.bases = this.postProcess(data);
    });
  }

  public tagsForBase(baseName: string): string[] {
    return this.baseByName[baseName].tags;
  }

  protected getJson(): Observable<BaseTemplate[]> {
    return this.http.get<BaseTemplate[]>("assets/bases.json");
  }

  protected postProcess(data: BaseTemplate[]): Base[] {
    this.bases = data.map(base => {
      const result = new Base(base);
      this.baseByName[result.base] = result;
      return result;
    });
    this.bases.sort((base1, base2) => {
      return base1.base.localeCompare(base2.base);
    });
    return this.bases;
  }
}
