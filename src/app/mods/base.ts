export interface BaseTemplate {
  base: string;
  tags: string[];
}

export class Base implements BaseTemplate {

  public base: string;
  public tags: string[];

  constructor(template: BaseTemplate) {
    Object.assign(this, template);
    this.tags.push("default");
  }

}
