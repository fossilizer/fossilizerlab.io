
export interface TagAndWeight {
  tag: string;
  weight: number;
}

export interface FossilTemplate {
  id: string;
  fullId: string;
  name: string;
  description: string;
  tagWeights: TagAndWeight[];
}

export class Fossil implements FossilTemplate {
  description: string;
  fullId: string;
  id: string;
  name: string;
  tagWeights: TagAndWeight[];

  constructor(template: FossilTemplate) {
    Object.assign(this, template);
  }

  get dropdownableName() {
    const result = `${this.name} (${this.oneLineDescription})`;
    return result;
  }

  get oneLineDescription() {
    return this.description.replace("\n", ", ");
  }

  public forbiddenTags(): string[] {
    return this.tagWeights.filter(tagWeight => {
      return tagWeight.weight === 0;
    }).map(tagWeight => {
      return tagWeight.tag;
    });
  }

  public likelyTags(): string[] {
    return this.tagWeights.filter(tagWeight => {
      return tagWeight.weight > 1;
    }).map(tagWeight => {
      return tagWeight.tag;
    });
  }

  public lessLikelyTags(): string[] {
    return this.tagWeights.filter(tagWeight => {
      return tagWeight.weight > 0 && tagWeight.weight <= 1;
    }).map(tagWeight => {
      return tagWeight.tag;
    });
  }

}
